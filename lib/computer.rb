# frozen_string_literal: true

require 'set'

class Computer
  attr_reader :size, :pc, :memory, :max_address, :stack_ptr

  INSTRUCTIONS = Set["MULT", "CALL", "RET", "STOP", "PRINT", "PUSH"]

  def initialize(size)
    if size <= 0 then
      raise StandardError.new("memory size can not be negative or zero")
    end

    @size = size
    @pc = 0
    @memory = Array.new(size)
    @max_address = 0
    @stack_ptr = 0
  end

  # set pc to the given address
  def set_address(address)
    if address < 0 then
      raise StandardError.new("address can not be negative")
    end

    if address >= @size then
      raise StandardError.new("address can not be bigger than the initial size")
    end

    @pc = address

    self
  end

  # only one value per instruction supported
  # TODO: validate if instruction expects value or not
  def insert(instruction, value = nil)
    unless INSTRUCTIONS.include?(instruction) then
      raise StandardError.new("unknown instruction: #{instruction}")
    end

    # insert instruction to the memory with the address of pc
    @memory[@pc] = [instruction, value]

    # update max_address so we know where does the stack start
    if @pc > @max_address then
      @max_address = @pc
    end

    # increase pc for the next instruction
    @pc += 1

    self
  end

  # execute the list of instructions
  def execute()
    @stack_ptr = @max_address # set stack pointer to the max address that has an instruction

    running = true

    while running
      if @pc > @max_address then
        raise StandardError.new("program counter overflow")
      end

      address_content = @memory[@pc]

      if address_content.nil? then
        raise StandardError.new("program counter tried to access an address without instruction")
      end

      # TODO: move instructions to different class to improve testability of each instruction
      case address_content[0]
      when "STOP"
        running = false
      when "PUSH"
        self.stack_push(address_content[1])
        @pc += 1
      when "PRINT"
        value = self.stack_pop()
        puts(value)
        @pc += 1
      when "MULT"
        val1 = self.stack_pop()
        val2 = self.stack_pop()
        self.stack_push(val1 * val2)
        @pc += 1
      when "CALL"
        @pc = address_content[1]
      when "RET"
        addr = self.stack_pop()
        @pc = addr
      end

    end

  end

  def stack_pop()
    if @stack_ptr <= max_address then
      raise StandardError.new("stack underflow")
    end

    value = @memory[@stack_ptr]
    @memory[@stack_ptr] = nil # "remove" old value
    @stack_ptr -= 1 # decrease stack size

    value
  end
  private :stack_pop

  def stack_push(value)
    @stack_ptr += 1 # increase stack size
    if @stack_ptr >= @size then
      raise StandardError.new("stack overflow")
    end

    @memory[@stack_ptr] = value
  end
  private :stack_push

end
